#!/usr/bin/perl

# evaluate input
while(<>){
    ($f,$t,$d,$k)=split ":::"; # get filename, title, date, keywordlist
    $f=~s/\.md$/\.html/; # replace .md with .html for correct links
    foreach $kw (split m/\s*,\s*/, $k) { # split the keywordlist
        $h->{$kw}->{$f}={"date"=>$d,"title"=>$t}; # create hash
        }
    };

# output html
print "<h1>all tags</h1>\n";
# loop over found keywords
foreach $kw (sort {keys %{$h->{$b}}<=>keys %{$h->{$a}}} sort keys %{$h}) {
    next if $kw =~ m/^\W*$/; # skip non-tagged pages

    print "<h2 class=\"mobile-center\" id=\"$kw\">#$kw</h2>\n";
    print "<ul class=\"postlist\">\n";
    map {print "<li><a href=\"$_\"><span class=\"hidden-mobile article-meta\">",$h->{$kw}->{$_}->{"date"}," </span>",$h->{$kw}->{$_}->{"title"},"</a></li>\n";
} sort keys %{$h->{$kw}};
    print "</ul>\n";
    }
