---
title: git workflow tips
author: Yann Büchau
date: 2016-12-28
keywords:
    - git
    - coding
    - software
---

# git workflow tips

![git and syncthing increase workflow productivity](../media/workflow-productivity-git-syncthing.jpg)

## Use aliases

Typing time matters! These aliases speed up your `git`{.bash} usage a fair bit:

```bash
## global git aliases ##
# shorten the most common git commands
# e.g. instead of 'git status', 'git s' is enough
git config --global alias.s = status
git config --global alias.c = commit
git config --global alias.ls = ls-files
git config --global alias.co = checkout
git config --global alias.b = branch
# output the git repository's root folder
git config --global alias.root = rev-parse --show-toplevel

## shell aliases ##
# alias 'cdgit' brings you back to your git repository's root folder
alias cdgit='cd $(git root)'
```


## A pretty log

To show a pretty tree-like branch graph, use this command:

```bash
git log --oneline --decorate --all --color --graph
```

Looks like this:
 
![`git log --oneline --decorate --all --color --graph`{.bash}](../media/git-tree-1.png)

If you want to see the whole commit message and information, skip the
`--oneline`{.bash} flag:

```bash
git log --decorate --all --color --graph
```

Which looks like this:

![`git log --decorate --all --color --graph`{.bash}](../media/git-tree-2.png)

You may of course define aliases for those commands:

```bash
git config --global alias.tree = 'log --decorate --all --color --graph'
git config --global alias.t = 'log --oneline --decorate --all --color --graph'
```

## Use `git pull --rebase`

When you have worked on a git repository with other people, you will most likely
have come across these annoying 'Merge commits'. You do a `git pull`{.bash} to
synchronize and suddenly a stupid Merge commit is necessary because someone
edited the same file as you.  This really messes up your history - see the
figure about the `git log ...`{.bash} above.
Fortunately, there is an easy solution to this. Instead of `git pull`{.bash}, do
a `git pull --rebase`{.bash}. This bases your changes onto the remote's new
commits that you weren't aware of yet. Make sure to have everything committed
before a `git pull --rebase`{.bash}, it will complain otherwise.



/nobodyinperson
