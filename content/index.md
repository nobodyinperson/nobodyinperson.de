---
title: nobodyinperson.de
author: Yann Büchau
date: 2016-10-01
---

# nobodyinperson.de

Welcome to my little webpage.

I am posting whatever comes to my mind is worth sharing, mostly concerning
my experiences with coding, tinkering and stuff.

Feel free to browse the [latest posts](#latestposts) below, the full 
[list of all posts](allposts.html) or the [tags page](alltags.html) for anything
that seems interesting to you.

:-)

/nobodyinperson
