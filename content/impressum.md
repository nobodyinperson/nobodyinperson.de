---
title: Legal disclosure
author: Yann Büchau
date: 2022-11-09
---

# Legal Disclosure

Information in accordance with section 5 TMG

<script>
document.write(
"Y","a","n","n"," ","B","ü","c","h","a","u",
"<br>",
"D","o","r","f","a","c","k","e","r","s","t","r","."," ","2","0",
"<br>",
"7","2","0","7","4"," ","T","ü","b","i","n","g","e","n",
"<br>","T","e","l","e","p","h","o","n","e",":"," ",
"+","4","9","1","5","7","8","4","6","0","6","1","8","0",
"<","b","r",">",
"E","-","M","a","i","l",":"," ",
"n","o","b","o","d","y","i","n","p","e","r","s","o","n","@","p","o","s","t","e","o",".","d","e"
)

</script>

## Accountability for content

The contents of these pages have been created with the utmost care. However, I
cannot guarantee the contents' accuracy, completeness or topicality. According
to statutory provisions, I am furthermore responsible for these own content on
these web pages. In this context, please note that I am accordingly not
obliged to monitor merely the transmitted or saved information of third
parties, or investigate circumstances pointing to illegal activity. these
obligations to remove or block the use of information under generally
applicable laws remain unaffected by this as per §§ 8 to 10 of the Telemedia
Act (TMG).

## Accountability for links

Responsibility for the content of external links (to web pages of third
parties) lies solely with the operators of the linked pages. No violations were
evident to me at the time of linking. Should any legal infringement become
known to me, I will remove the respective link immediately.

## Copyright

These web pages and their contents are subject to German copyright law. Unless
expressly permitted by law (§ 44a et seq. of the copyright law), every form of
utilizing, reproducing or processing works subject to copyright protection on
these web pages requires the prior consent of the respective owner of the
rights.  Unauthorized utilization of copyrighted works is punishable (§ 106 of
the copyright law).

Source: impressum generator - [twigg.de](http://twigg.de)

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0;" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Unless otherwise stated, the content on this site is licensed under the <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

