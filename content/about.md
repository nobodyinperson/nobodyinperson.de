---
title: About me
author: Yann Büchau
date: 2018-03-27
---

# About me

My name is **Yann Büchau**, but everywhere I am on the internet my nickname is
just **nobodyinperson**.

I am a German meteorology PhD student at the University of Tübingen.

My hobbies are coding, tinkering and playing guitar. Currently, I like
**Python, Perl, R and shell programming** the most. I am also very much into
**raspberry pi** stuff.

Since end of school, I use **GNU/Linux** for everything. I love having a
Linux-based system right in my pocket, hence I am a proud owner of a **Nokia
N9** and a **Jolla** phone.

My gpg key id is **1E1F9E32**.

:-)

/nobodyinperson or yann

