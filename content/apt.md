---
title: apt repository
author: Yann Büchau
date: 2016-10-03
keywords:
    - apt
    - dpkg
    - debian
---

# apt repository

I am hosting an apt repository here to simplify distribution of my software
across my machines.

If you are interested in how to host such an apt repository yourself, take a
look at
[reprepro](https://wiki.debian.org/SettingUpSignedAptRepositoryWithReprepro).

> ## Disclaimer
>
> Use any software from this repository **at your own risk**!
>
> Everything in this repository is from my open-source projects somewhere on
> [GitLab](https://gitlab.com) or [GitHub](https://github.com).
> I am using everything in this repository myself ~~but be aware that this is
> still an **insecure HTTP** connection!~~ and meanwhile (27.12.2017), I finally secured
> this site's communication with **HTTPS** but I still cannot guarantee for
> **anything**!
>
> ### You have been warned!

To be able to install software from this repository on your machine, run the
following commands:

```bash
# add the gpg key
wget -qO- https://apt.nobodyinperson.de/gpg.key | sudo apt-key add -

# add the apt sources list entry
echo "deb https://apt.nobodyinperson.de unstable main" | sudo tee /etc/apt/sources.list.d/nobodyinperson.list

# update repositories
sudo apt-get update

# [OPTIONAL] to get a list of the available packages (names only)
awk '$1 == "Package:" { print $2 }' /var/lib/apt/lists/*nobodyinperson*main*$(dpkg --print-architecture)*
# [OPTIONAL] or with more information
cat /var/lib/apt/lists/*nobodyinperson*main*$(dpkg --print-architecture)*

# You may then install software from this repository via
sudo apt-get install PACKAGENAME
```

/nobodyinperson
