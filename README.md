# nobodyinperson.de

This is the **full** source for my website
[nobodyinperson.de](http://nobodyinperson.de).

## How does it work?

A **Makefile** instructs **pandoc** and some other tools to create **static html files**
from **markdown** and stuffs everything the website needs in a folder.

## Background

I wanted a simple, minimalistic blog. Searching the web for blog platforms, I didn't find any that really did it for me. Something then pointed me to [W. Caleb McDaniel's website repo](https://github.com/wcaleb/website) and I was quite taken with the idea of creating static websites from markdown via pandoc.

I am a fan of having things portable, so I decided to set up a system that takes simple markdown files and produces a self-contained folder with everything the whole website needs. This folder can then be put on **any** webserver that is able to serve static files and the website works.

## Use it yourself

Feel free to [fork this repository](https://gitlab.com/nobodyinperson/nobodyinperson.de/forks/new) and adjust it to your needs:

- the ```content/*``` files
- the template ```templates/nobodyinperson.de```
- the css stylesheet ```css/main.css```

Run ```make``` to create the ```build``` folder, the contents of which you may copy to your preferred destination by ```make install```. Make sure to adjust ```INSTALLDIR``` in the ```Makefile``` to your needs.

Point your browser to the subdirectory ```content``` of the ```INSTALLDIR``` on your webserver and you have a minimalistic blog! :-)

/nobodyinperson
