#/usr/bin/env make -f
# Makefile 
# to create the whole webpage directory

#################
### Variables ###
#################

#################
## directories ##
#################
# the build directory
# the self-contained website structure will be created there,
# meaning pointing a browser to this path will give you the full website.
BUILDDIR = build

# the temporary directory
TEMPDIRNAME = temp
TEMPDIR = $(BUILDDIR)/$(TEMPDIRNAME)

# installation directory
# The build directory contents will be copied into this directory,
# meaning that pointing a browser to this path/domain will give you the full
# website.
INSTALLDIR = /var/www/website

# the directory with scripts
SCRIPTSDIR = scripts

# The content directory
CONTENTDIRNAME = content
CONTENTDIR = $(CONTENTDIRNAME)
CONTENTBUILDDIR = $(BUILDDIR)/$(CONTENTDIRNAME)

# all input markdown files
CONTENT   = $(shell find $(CONTENTDIRNAME) -type f -iname '*.md')
# the corresponding html files
HTMLFILES = $(addprefix $(CONTENTBUILDDIR)/, $(notdir  $(CONTENT:.md=.html)))

# the templates directory
TEMPLATESDIR = templates

# the pandoc template
WEBSITETEMPLATE = $(TEMPLATESDIR)/nobodyinperson.de.html
KEYWORDSTEMPLATE = $(TEMPLATESDIR)/keywords.pdc

# postlists
POSTLISTTEMPLATE = $(TEMPLATESDIR)/postlist.pdc
LATESTPOSTSLISTHTML  = latest_posts_list.html
ALLPOSTSLISTHTML = all_posts_list.html

# taglists
ALLKEYWORDSMD   = $(TEMPDIR)/alltags.md
ALLKEYWORDSPAGE = $(CONTENTBUILDDIR)/alltags.html

# number of posts being "latest"
NUM_LATESTPOSTS = 3

# name of allpostlist document
ALLPOSTSPAGE   = allposts.html

# options for pandoc
PANDOCOPTS = -f markdown -t html5 --standalone \
	--template=$(WEBSITETEMPLATE) \
	--css=../css/main.css \
	--variable latestpostlistfile=$(LATESTPOSTSLISTHTML)


###############
### Targets ###
###############

%: Makefile

# build everything
.PHONY: all
all: media css lib $(ALLKEYWORDSPAGE) $(CONTENTBUILDDIR)/$(ALLPOSTSPAGE) $(HTMLFILES)

# create the build directory
$(BUILDDIR):
	mkdir -p $(BUILDDIR)

# create the temporary directory
$(TEMPDIR): | $(BUILDDIR)
	mkdir -p $(TEMPDIR)

# create the content directory with built html pages
$(CONTENTBUILDDIR): | $(BUILDDIR)
	mkdir -p $(CONTENTBUILDDIR)

# translate all markdown files to html
$(CONTENTBUILDDIR)/%.html: $(CONTENTDIR)/%.md $(WEBSITETEMPLATE) | $(CONTENTBUILDDIR) 
	pandoc $(PANDOCOPTS) -o $@ $<

# create list of all posts
$(TEMPDIR)/$(ALLPOSTSLISTHTML): $(CONTENT) $(POSTLISTTEMPLATE) | $(TEMPDIR) 
	find content -type f -iname '*.md' \
		| while IFS='\n' read mdfile;do pandoc -t html5 --template=/home/yann/code/nobodyinperson.de/templates/postlist.pdc --variable link="$$mdfile" "$$mdfile" | tr '\n' ' ';echo;done \
		| sort -r | cut -d" " -f2- | perl -pe 's/\.md/.html/g' \
	> $(TEMPDIR)/$(ALLPOSTSLISTHTML)

# create list of latest posts
$(CONTENTBUILDDIR)/$(LATESTPOSTSLISTHTML): $(TEMPDIR)/$(ALLPOSTSLISTHTML) | $(CONTENTBUILDDIR)
	head -n $(NUM_LATESTPOSTS) $(TEMPDIR)/$(ALLPOSTSLISTHTML) > $(CONTENTBUILDDIR)/$(LATESTPOSTSLISTHTML)

# create html document of all posts
# first, wirte the html list into a markdown document with a headline
# then convert it to html
ALLPOSTSPAGEMD = allposts.md
$(CONTENTBUILDDIR)/$(ALLPOSTSPAGE) $(TEMPDIR)/$(ALLPOSTSPAGEMD): $(TEMPDIR)/$(ALLPOSTSLISTHTML) $(WEBSITETEMPLATE) $(CONTENTBUILDDIR)/$(LATESTPOSTSLISTHTML) | $(CONTENTBUILDDIR) $(TEMPDIR)
	echo -e "# all posts\n<ul class=\"postlist\">\n$$(cat $(TEMPDIR)/$(ALLPOSTSLISTHTML))\n</ul>" > $(TEMPDIR)/$(ALLPOSTSPAGEMD)
	pandoc $(PANDOCOPTS) -o $(CONTENTBUILDDIR)/$(ALLPOSTSPAGE) $(TEMPDIR)/$(ALLPOSTSPAGEMD)

# create markdown/html page of all tags
$(ALLKEYWORDSMD): $(CONTENT) | $(TEMPDIR)
	find content -type f -iname '*.md' -execdir \
	pandoc -f markdown -t html5 -V filename={} --template=$(abspath $(KEYWORDSTEMPLATE)) {} \; \
	| perl $(SCRIPTSDIR)/create-keywordpagemd.pl \
	> $(ALLKEYWORDSMD)
	
# create full html page of all tags
$(ALLKEYWORDSPAGE): $(ALLKEYWORDSMD) | $(CONTENTBUILDDIR)
	pandoc $(PANDOCOPTS) -o $(ALLKEYWORDSPAGE) $(ALLKEYWORDSMD)

# copy the media files
# unfortunately, copying changes the build directory's modification time,
# so we have to manually reset it to something very old to prevent some
# rules like the markdown->html conversion to be triggered unecessaryly again.
# The most youngest but still oldest time I could imaging was the time
# of this Makefile. :-)
.PHONY: media
media: | $(BUILDDIR)
	rsync -ur media $(BUILDDIR)

# copy the css stylesheets
# unfortunately, copying changes the build directory's modification time,
# so we have to manually reset it to something very old to prevent some
# rules like the markdown->html conversion to be triggered unecessaryly again.
# The most youngest but still oldest time I could imaging was the time
# of this Makefile. :-)
.PHONY: css
css: | $(BUILDDIR)
	rsync -ur css $(BUILDDIR)

# copy the library files
# unfortunately, copying changes the build directory's modification time,
# so we have to manually reset it to something very old to prevent some
# rules like the markdown->html conversion to be triggered unecessaryly again.
# The most youngest but still oldest time I could imaging was the time
# of this Makefile. :-)
.PHONY: lib
lib: | $(BUILDDIR)
	rsync -ur lib $(BUILDDIR)

.PHONY: clean
clean:
	rm -rf $(BUILDDIR)

$(INSTALLDIR):
	mkdir -p $(INSTALLDIR)

.PHONY: install
install: all $(INSTALLDIR)
	rsync -r --exclude=/$(TEMPDIRNAME) $(BUILDDIR)/* $(INSTALLDIR)

.PHONY: remove
remove:
	rm -rf $(INSTALLDIR)

